import { Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { remote } from 'electron';
import moment from 'moment';
import { handleMouseEnter, handleMouseLeave } from '../../utils';

interface TimeDisplayComponentProps {
    date?: Date;
}

const TimeDisplayComponent: React.FC<TimeDisplayComponentProps> = props => {
    const date = Date.now() || props.date;
    const [thisMoment, setThisMoment] = useState(moment(date));

    useEffect(() => {
        if (props.date) return;
        const interval = window.setInterval(() => {
            setThisMoment(() => moment());
        }, 500);
        return () => {
            window.clearInterval(interval);
        };
    }, []);
    return (
        <div className="TimeDisplayComponent">
            <Typography color="secondary" variant="subtitle2">
                {thisMoment.format('MMMM Do YYYY, h:mm:ss a, dddd DDDo wo YYYYYY, X')}
            </Typography>
        </div>
    );
};

export default TimeDisplayComponent;
