import { remote } from 'electron';
import React, { useEffect, useRef, useState } from 'react';
import Point from '../../point';
import './styles.css';

const PixelRulerComponent: React.FC = () => {
    const canvas = useRef<HTMLCanvasElement | null>(null);
    const [beginPoint, setBeginPoint] = useState<Point | null>(null);
    const [endPoint, setEndPoint] = useState<Point | null>(null);
    const [pathing, setPathing] = useState(false);

    const handleMouseMove = (e: MouseEvent) => {
        if (!canvas.current) return;
        const ctx = canvas.current.getContext('2d');
        if (!ctx) return;
        setPathing(pathing => {
            if (pathing) ctx.clearRect(0, 0, canvas.current!.width, canvas.current!.height);
            return pathing;
        });
        setBeginPoint(beginPoint => {
            if (!beginPoint) return beginPoint;
            ctx.fillStyle = 'green';
            ctx.beginPath();
            ctx.arc(beginPoint.x, beginPoint.y, 5, 0, 2 * Math.PI, false);
            ctx.fill();
            ctx.closePath();
            ctx.strokeStyle = 'blue';
            ctx.beginPath();
            ctx.lineWidth = 2;
            ctx.moveTo(beginPoint.x, beginPoint.y);
            ctx.lineTo(e.x, e.y);
            ctx.stroke();
            ctx.closePath();
            return beginPoint;
        });
    };

    const handleMouseDown = (e: MouseEvent) => {
        if (!canvas.current) return;
        const ctx = canvas.current.getContext('2d');
        if (!ctx) return;
        ctx.fillStyle = 'green';
        ctx.clearRect(0, 0, canvas.current.width, canvas.current.height);
        setBeginPoint(e);
        ctx.beginPath();
        ctx.arc(e.x, e.y, 5, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.closePath();
        setPathing(true);
    };

    const handleMouseUp = (e: MouseEvent) => {
        if (!canvas.current) return;
        const ctx = canvas.current.getContext('2d');
        if (!ctx) return;
        ctx.clearRect(0, 0, canvas.current.width, canvas.current.height);
        ctx.fillStyle = 'red';
        ctx.beginPath();
        ctx.arc(e.x, e.y, 5, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.closePath();
        setEndPoint(e);
        setBeginPoint(beginPoint => {
            if (!beginPoint) return beginPoint;
            ctx.fillStyle = 'green';
            ctx.beginPath();
            ctx.arc(beginPoint.x, beginPoint.y, 5, 0, 2 * Math.PI, false);
            ctx.fill();
            ctx.closePath();
            ctx.beginPath();
            ctx.strokeStyle = 'blue';
            ctx.lineWidth = 2;
            ctx.moveTo(beginPoint.x, beginPoint.y);
            ctx.lineTo(e.x, e.y);
            ctx.stroke();
            ctx.closePath();
            return null;
        });
        setPathing(false);
    };

    useEffect(() => {
        canvas.current!.width = window.innerWidth;
        canvas.current!.height = window.innerHeight;

        remote.getCurrentWindow().setIgnoreMouseEvents(false);

        canvas.current?.addEventListener('mousemove', handleMouseMove);
        canvas.current?.addEventListener('mousedown', handleMouseDown);
        canvas.current?.addEventListener('mouseup', handleMouseUp);
        return () => {
            canvas.current?.removeEventListener('mousemove', handleMouseMove);
            canvas.current?.removeEventListener('mousedown', handleMouseDown);
            const ctx = canvas.current?.getContext('2d');
            ctx?.clearRect(0, 0, canvas.current!.width, canvas.current!.height);
            canvas.current?.removeEventListener('mouseup', handleMouseUp);
            remote.getCurrentWindow().setIgnoreMouseEvents(true, { forward: true });
        };
    }, [canvas]);

    return (
        <div className="PixelRulerComponent">
            <canvas className="PixelRulerComponentCanvas" ref={canvas} />
        </div>
    );
};

export default PixelRulerComponent;
