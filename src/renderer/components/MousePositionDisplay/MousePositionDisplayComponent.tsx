import { Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import Point from '../../point';

const MousePositionDisplayComponent: React.FC = () => {
    const [mousePosition, setMousePosition] = useState({ x: 0, y: 0 } as Point);
    useEffect(() => {
        const handleMouseMove = (e: MouseEvent) => {
            setMousePosition(e);
        };
        document.addEventListener('mousemove', handleMouseMove);
        return () => {
            document.removeEventListener('mousemove', handleMouseMove);
        };
    });
    return (
        <div className="mousePositionDisplayComponent">
            <Typography variant="subtitle2" color="secondary">
                {`(${mousePosition.x}, ${mousePosition.y})`}
            </Typography>
        </div>
    );
};

export default MousePositionDisplayComponent;
