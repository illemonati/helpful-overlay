import { Typography } from '@material-ui/core';
import TimeDisplayComponent from '../TimeDisplay/TimeDisplayComponent';
import React, { useEffect, useRef, useState } from 'react';
import './styles.css';
import MousePositionDisplayComponent from '../MousePositionDisplay/MousePositionDisplayComponent';
import PixelRulerComponent from '../PixelRuler/PixelRulerComponent';
import { remote } from 'electron';
import HaruhiComponent from '../Haruhi/HaruhiComponent';

const MainOverlayComponent: React.FC = () => {
    const [pixelRulerOn, setPixelRulerOn] = useState(false);
    const [haruhiOn, setHaruhiOn] = useState(false);
    // const [haruhiUntouchable, setHaruhiUntouchable] = useState(false);
    const haruhiUntouchable = useRef(false);
    // const haruhiFullScreen = useRef(false);
    const [haruhiFullScreen, setHaruhiFullScreen] = useState(false);

    function useForceUpdate() {
        const [value, setValue] = useState(0); // integer state
        return () => setValue(value => ++value); // update the state to force render
    }
    const forcedUpdate = useForceUpdate();
    useEffect(() => {
        remote.globalShortcut.register('CommandOrControl+Shift+R', () => {
            setPixelRulerOn(state => !state);
        });
        remote.globalShortcut.register('CommandOrControl+Shift+H', () => {
            setHaruhiOn(state => !state);
        });
        remote.globalShortcut.register('CommandOrControl+Shift+U', () => {
            haruhiUntouchable.current = !haruhiUntouchable.current;
            forcedUpdate();
        });
        remote.globalShortcut.register('CommandOrControl+Shift+T', () => {
            // console.log(!haruhiFullScreen.current);
            setHaruhiFullScreen(s => !s);
        });
        return () => {
            remote.globalShortcut.unregisterAll();
        };
    }, []);
    return (
        <div className="MainOverlayComponent">
            <div className="HaruhiDisplay" style={{ display: haruhiOn ? '' : 'none' }}>
                <HaruhiComponent
                    haruhiUntouchable={haruhiUntouchable}
                    haruhiFullScreen={haruhiFullScreen}
                />
            </div>
            <div className="CornerTimeDisplay">
                <TimeDisplayComponent />
            </div>
            <div className="MousePositionDisplay">
                <MousePositionDisplayComponent />
            </div>
            {pixelRulerOn && <PixelRulerComponent />}
        </div>
    );
};

export default MainOverlayComponent;
