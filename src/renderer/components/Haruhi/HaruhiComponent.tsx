import React, {
    CSSProperties,
    MutableRefObject,
    useCallback,
    useContext,
    useEffect,
    useRef,
    useState
} from 'react';
import fileUrl from 'file-url';
import { remote } from 'electron';
import './styles.css';
import { Button, Container, Paper, Select, Slider } from '@material-ui/core';
import fs from 'fs';
import path from 'path';

const videoUrls = {} as { [key: string]: string };
const ctx = require.context('./videos', true, /\.mp4$/);
for (const key of ctx.keys()) {
    videoUrls[key] = ctx(key);
}

let ogOpa = 1;

interface HaruhiProps {
    haruhiUntouchable: MutableRefObject<boolean>;
    haruhiFullScreen: boolean;
}

const HaruhiComponent: React.FC<HaruhiProps> = (props: HaruhiProps) => {
    const video = useRef<HTMLVideoElement | null>(null);
    const haruhiPaper = useRef<Element | null>(null);
    const [videoUrlIndex, setVideoUrlIndex] = useState(0);
    const [userVideos, setUserVideos] = useState({} as { [key: string]: string });
    const [haruhiPaperStyle, setHaruhiPaperStyle] = useState({
        backgroundColor: 'rgba(0, 0, 0, 0)'
    });

    const fullScreenStyle = {
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        top: 0,
        transform: 'translate(-50%, -50%)'
    } as CSSProperties;
    const haruhiUntouchable = props.haruhiUntouchable;
    const haruhiFullScreen = props.haruhiFullScreen;
    const [haruhiVideoOpacity, setHaruhiVideoOpacity] = useState(ogOpa);

    const controlsOn = useCallback(() => {
        return !haruhiUntouchable.current;
    }, [haruhiUntouchable.current]);

    const refreshUserVideos = async () => {
        console.log(process.version);
        const documentsPath = remote.app.getPath('documents');
        const userVideosPath = path.join(documentsPath, 'helpful-overlay/', 'videos/');
        if (!fs.existsSync(userVideosPath)) {
            fs.mkdirSync(userVideosPath, { recursive: true });
        }
        const files = fs
            .readdirSync(userVideosPath, { withFileTypes: true })
            .filter(file => file.isFile());
        setUserVideos(() => {
            const newVideos = {} as { [key: string]: string };
            for (const item of files) {
                newVideos[item.name] = fileUrl(path.join(userVideosPath, item.name));
            }
            return newVideos;
        });
    };
    //@ts-ignore
    // window.location = 'chrome://gpu';
    const cleanUp = () => {
        remote.getCurrentWindow().setIgnoreMouseEvents(true, { forward: true });
        remote.getCurrentWindow().blur();
    };
    const over = () => {
        if (haruhiUntouchable.current) return;

        remote.getCurrentWindow().setIgnoreMouseEvents(false);
        remote.getCurrentWindow().focus();
    };

    const handleHaruhiVideoOpacityChange = (e: any, newNumber: number | number[]) => {
        let newOpacity = newNumber as number;
        if (newOpacity > 1) newOpacity = 1;
        if (newOpacity < 0.03) newOpacity = 0.03;
        if (!newOpacity) return;
        setHaruhiVideoOpacity(() => newOpacity);
    };

    const handleVideoChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        const newVal = event.target.value as number;
        setVideoUrlIndex(() => newVal);
    };

    const totalVideosLength = () => Object.keys(videoUrls).length + Object.keys(userVideos).length;
    const getSrc = (index: number) => {
        console.log(userVideos);
        if (index < Object.keys(videoUrls).length) {
            return videoUrls[Object.keys(videoUrls)[videoUrlIndex]];
        }
        console.log(videoUrlIndex, videoUrlIndex - Object.keys(videoUrls).length);
        console.log(Object.keys(userVideos)[videoUrlIndex - Object.keys(videoUrls).length]);
        return userVideos[Object.keys(userVideos)[videoUrlIndex - Object.keys(videoUrls).length]];
    };
    useEffect(() => {
        refreshUserVideos();
    }, []);

    useEffect(() => {
        if (!haruhiPaper.current) return;
        haruhiPaper.current.addEventListener('mouseover', over);
        haruhiPaper.current.addEventListener('mouseleave', cleanUp);
        return () => {
            if (!haruhiPaper.current) return;
            haruhiPaper.current.removeEventListener('mouseover', over);
            haruhiPaper.current.removeEventListener('mouseleave', cleanUp);
            cleanUp();
        };
    }, [haruhiPaper]);

    useEffect(() => {
        const currentVideo = video.current;
        const handler = () => {
            setVideoUrlIndex(i => {
                setTimeout(() => currentVideo?.play(), 2000);
                return i >= totalVideosLength() - 1 ? 0 : i + 1;
            });
        };
        currentVideo?.addEventListener('ended', handler);
        currentVideo?.addEventListener('error', handler);
        return () => {
            currentVideo?.removeEventListener('ended', handler);
            currentVideo?.removeEventListener('error', handler);
        };
    }, [video.current]);
    return (
        <div className="HaruhiComponent">
            <Paper
                className="haruiPaper"
                style={haruhiPaperStyle}
                ref={haruhiPaper}
                variant="elevation"
                elevation={0}
            >
                <div className="haruhiControlDiv" style={{ opacity: haruhiVideoOpacity }}>
                    <Container>
                        <Select value={videoUrlIndex} onChange={handleVideoChange} native>
                            <optgroup label="Built In">
                                {Object.keys(videoUrls).map((videoUrlName, i) => {
                                    return (
                                        <option key={i} value={i}>
                                            {videoUrlName}
                                        </option>
                                    );
                                })}
                            </optgroup>
                            <optgroup label="User Videos">
                                {Object.keys(userVideos).map((videoUrlName, i) => (
                                    <option
                                        key={i + Object.keys(videoUrls).length}
                                        value={i + Object.keys(videoUrls).length}
                                    >
                                        {videoUrlName}
                                    </option>
                                ))}
                            </optgroup>
                        </Select>
                        <Button onClick={refreshUserVideos}>Refresh</Button>
                        <br />
                        <Slider
                            max={1}
                            min={0}
                            step={0.01}
                            value={haruhiVideoOpacity}
                            onChange={handleHaruhiVideoOpacityChange}
                        ></Slider>
                    </Container>
                </div>
                <br />
                <video
                    className="haruhiVideo"
                    ref={video}
                    style={(() => {
                        if (!haruhiFullScreen)
                            return {
                                opacity: haruhiVideoOpacity
                            };
                        return {
                            opacity: haruhiVideoOpacity,
                            ...fullScreenStyle
                        };
                    })()}
                    src={getSrc(videoUrlIndex)}
                    controls={controlsOn()}
                ></video>
            </Paper>
        </div>
    );
};

export default HaruhiComponent;
